from wordcloud import WordCloud


def visualize(entries: list):
    text = ' '.join(entries)
    wc = WordCloud(width=1080, height=1080, repeat=True).generate(text)
    wc.to_file('../static/images/out.png')


if __name__ == "__main__":
    todos = ['studies', 'personal', 'plans', '2020-3-04-kva',
             '2020-3-15-mur', '2020-3-07-kos', '2020-3-17-nik']
    visualize(todos)
