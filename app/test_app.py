from fastapi.testclient import TestClient
import pytest
from app import app
from app import todo_add
from database import init_db, get_db, Session
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from fastapi import Depends
import datetime
from models import Base
import pandas as pd
import models

client = TestClient(app)


def test_create_todo(database = next(get_db())):
    head = {'Content-Type': 'application/x-www-form-urlencoded'}
    request_data = {
                    'title': 'test_todo'
                    }
    with TestClient(app) as client:
        response = client.post("/add", request_data, head)
        todo = database.query(models.Todo).filter(models.Todo.title == 'test_todo').first()
        assert response.status_code == 303
        assert todo.title == 'test_todo'
        assert todo.tag == ''


def test_list_endpoint():
    with TestClient(app) as client:
        response = client.get("/list")
        assert response.status_code == 200


def test_add_todo(database = next(get_db())):
    head = {'Content-Type': 'application/x-www-form-urlencoded'}
    request_data = {
                    'id': 0
                    }
    with TestClient(app) as client:
        response_add = client.post("/add", request_data, head)
        todo = database.query(models.Todo).filter(models.Todo.title == 'test_todo').first()
        response_delete = client.get("/delete/"+str(todo.id))
        assert response_delete.status_code == 200


def test_importlist():
    with TestClient(app) as client:
        response = client.get("/importlist")
        assert response.status_code == 200


@pytest.fixture
def test_excel():
    data = pd.DataFrame.from_dict({"id": 0,
                                   "title": "test",
                                   "details": "details of test",
                                   "completed": True,
                                   "tag": "personal",
                                   "creation_date": "01-01-2020",
                                   "completion_date": "10-01-2020",
                                   "fullname": "2020-3-15-mur"
                                   })

    data.to_excel('test_data.xlsx', sheet_name='todos', index=False)
    return "test_data.xlsx"
def test_import(database = next(get_db())):
    with TestClient(app) as client:
        response = client.get(f"/import?file_name={test_excel}")
        todo = database.query(models.Todo).filter(models.Todo.id == '1000').first()
        assert response.status_code == 200
        assert  todo.title == "test"
        assert  todo.full == "2020-3-15-mur"




