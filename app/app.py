"""Main of todo app
"""
import json

# TO-DO: Скрипт генерации, разобраться с постман, разобраться с Фастапи,
# на странице редактирование кнопку удалить,
# при этом кнопку сохранить оставляет на странице


import os, shutil
from PIL import Image
from fastapi import Cookie
from fastapi.responses import JSONResponse
import subprocess
from bs4 import BeautifulSoup
import requests
import urllib
import math
import gitlab
import datetime
import tkinter
import random
from random import randint
from tkinter import filedialog
from typing import Union
# pylint: disable=import-error
from loguru import logger
# pylint: disable=import-error
from elasticsearch import Elasticsearch
from fastapi import FastAPI, Request, Depends, Form, status, HTTPException, Response
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from starlette.responses import FileResponse
import pandas as pd
from database import init_db, get_db, Session
from auth import Authorization
import models
from PIL import Image, ImageChops,ImageStat
import os
import imagehash

init_db()

# pylint: disable=invalid-name
templates = Jinja2Templates(directory="templates")

app = FastAPI()

logger = logger.opt(colors=True)
# pylint: enable=invalid-name

app.mount("/static", StaticFiles(directory="static"), name="static")

imported_files = []
settings = {
        "settings": {
            "analysis": {
                "filter": {
                    "russian_stop": {
                        "type": "stop",
                        "stopwords": "_russian_"
                    },
                    "custom_stopwords": {
                        "type": "stop",
                        "stopwords": ["князь", "повезет", "сорок"]
                    }
                },
                "analyzer": {
                    "custom_russian": {
                        "type": "custom",
                        "tokenizer": "standard",
                        "char_filter": [
                            "html_strip"
                        ],
                        "filter": [
                            "lowercase",
                            "russian_stop",
                            "custom_stopwords",
                            "snowball",
                        ]
                    }
                }
            }
        },
        "mappings": {
            "properties": {
                "id": {
                    "type": "integer",
                },
                "title": {
                    "type": "text",
                    "analyzer": "custom_russian"
                },
                "completed": {
                    "type": "boolean",
                },
                "details": {
                    "type": "text",
                    "analyzer": "custom_russian"
                },

                "tag": {
                    "type": "text",
                    "analyzer": "custom_russian"
                },

                "creation_date": {
                    "type": "date",
                },

                "completion_date": {
                    "type": "date",
                },

                "fullname": {
                    "type": "text",
                    "analyzer": "custom_russian"
                }
            }
        }
    }


def difference_images(img1: str, img2: str):
    image_1 = Image.open(img1)
    image_2 = Image.open(img2)
    hash1 = imagehash.average_hash(image_1)
    hash2 = imagehash.average_hash(image_2)

    if hash1 == hash2:
        res = img2[img2.rfind('/')+1:img2.rfind('.')]
        print(f"path: {img2}")
    else:
        res = ""
    return res

@app.get("/")
async def home(request: Request, token: Union[str, None] = None):
    """Main page with todo list
    """
    es = Elasticsearch("http://lr2_elasticsearch_1:9200")
    if es.ping():
        logger.info("Connect successful!")
    else:
        logger.info("ERROR: could not connect")

    if es.indices.exists(index="2020-3-04-kva"):
        print("Index already exist!")
    else:
        es.indices.create(index="2020-3-04-kva", body=settings)
        print("Index created!")

    logger.info(f"[TOKEN]: {token}")
    return templates.TemplateResponse("index.html", {"request": request, "token": token})


@app.get("/visualize")
async def visualize(request: Request, token: Union[str, None] = None):
    """Main page with todo list
    """
    return templates.TemplateResponse("visual.html", {"request": request})


def set_lim(count: int, request: Request, response: Response, limit: int | None = Cookie()):
    response.set_cookie(key="limit", value=count)


@app.get("/set_limit/")
async def set_cookies(count: int, request: Request, response: Response, limit: int | None = Cookie(),
                      ):
    response = RedirectResponse(url="/")
    response.set_cookie(key="limit", value=count, domain="172.20.0.1")
    return response


# limit: int | None = Cookie(default=None)
@app.get("/list")
async def todo_list(response: Response, request: Request, skip: Union[int, None] = None,
                    skipp: int | None = Cookie(default=None), database: Session = Depends(get_db),
                    limit: int | None = Cookie(default=None), token: Union[str, None] = None):
    """Main page with todo list
    """
    if skip is None:
        # skip = skipp
        skip = 0
    if limit is None:
        limit = 5
    count = database.query(models.Todo).count()
    fullnames = {"2020-3-04-kva": [],
                 "2020-3-15-mur": [],
                 "2020-3-07-kos": [],
                 "2020-3-18-nik": [],
                 "Общее": []}

    logger.info("In home")
    todos = database.query(models.Todo).order_by(models.Todo.full) \
        .offset(skip * limit).limit(limit)

    for i in models.Fullname:
        tmp = [j for j in todos if j.full == i]
        fullnames[i] = tmp

    pages = math.ceil(count / limit)
    if skip > pages:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No such page")
    template_response = templates.TemplateResponse("list.html", context={"request": request, "todos": todos,
                                                                         "page": skip, "pages": pages,
                                                                         "max": limit, "fullnames": fullnames,
                                                                         "tags": models.Tags, "token": token})
    template_response.set_cookie(key="skipp", value=skip)
    return template_response
    # return templates.TemplateResponse("list.html", {"request": request, "todos": todos,
    #                                                "page": skip, "pages": pages,
    #                                                "max": limit, "fullnames": fullnames,
    #                                                "tags": models.Tags, "token": token})


@app.get("/edit/{todo_id}")
async def todo_get(request: Request, todo_id: int, database: Session = Depends(get_db), token: Union[str, None] = None):
    """Get todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    logger.info(f"Getting todo: {todo}")
    return templates.TemplateResponse("edit.html", {"request": request,
                                                    "todo": todo, "tags": models.Tags,
                                                    "fullname": models.Fullname,
                                                    "token": token})


@app.post("/add")
async def todo_add(title: str = Form(default=" "), token: Union[str, None] = None,
                   database: Session = Depends(get_db), tag: str = Form(None)):
    """Add new todo
    """
    #if token != "None":
    if title == " ":
        raise HTTPException(status_code=404, detail="Title is empty")
    todo = models.Todo(title=title, tag=tag, author=token.lstrip("tkn"))
    logger.info(f"Creating todo: {todo}")
    todo.creation_date = datetime.date.today().strftime("%d-%m-%Y")
    database.add(todo)
    database.commit()
    return RedirectResponse(url=app.url_path_for(name="home") + f"?token={token}",
                            status_code=status.HTTP_303_SEE_OTHER)
    #else:
    #    logger.info("Not permitted")
     #   return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/import/issues")
async def import_issues(request: Request, url: str, token: str, database: Session = Depends(get_db)):
    token = token
    gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
    gl.auth
    # glpat-RFteYqR_5hDWVLNFUw9v
    commandlisten = ("curl", "-XGET", "--header", "PRIVATE-TOKEN: " + token + "",
                     "https://gitlab.com/api/v4/projects?owned=true")
    py = subprocess.run(commandlisten, capture_output=True, text=True)

    sy = py.stdout

    index = sy.find(url)

    rindex = sy.rfind("id", 0, index)
    id = sy[rindex + 4:rindex + 12]
    print(id)
    project = gl.projects.get(id)

    issues = project.issues.list()
    for issue in issues:
        todo = models.Todo(title=issue.title, details=issue.description, creation_date=issue.created_at)
        database.add(todo)
        database.commit()
    return templates.TemplateResponse("index.html", {"request": request}
                                      )


@app.get("/complete/{todo_id}")
async def todo_comp(todo_id: int, database: Session = Depends(get_db), token: Union[str, None] = None):
    """Complete or uncomplete todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    todo.completed = not todo.completed
    if todo.completed:
        todo.completion_date = datetime.date.today().strftime("%d-%m-%Y")
    database.commit()
    return RedirectResponse(url=app.url_path_for("home") + f"?token={token}", status_code=status.HTTP_303_SEE_OTHER)


# pylint: disable=too-many-arguments
@app.post("/edit/{todo_id}")
async def todo_edit(todo_id: int, title: str = Form(max_length=500), completed: bool = Form(False),
                    database: Session = Depends(get_db), tag: str = Form(None), full: str = Form(None),
                    details: str = Form(...), token: Union[str, None] = None):
    """Edit todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    logger.info(f"Editting todo: {todo}")
    todo.details = details
    todo.title = title
    todo.completed = completed
    todo.tag = tag
    todo.full = full
    database.commit()

    return RedirectResponse(url=app.url_path_for("home") + f"?token={token}", status_code=status.HTTP_303_SEE_OTHER)


@app.get("/delete/{todo_id}")
async def todo_delete(todo_id: int, database: Session = Depends(get_db)):
    """Delete todo
    """
    try:
        todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
        logger.info(f"Deleting todo: {todo}")
        database.delete(todo)
        database.commit()
    except Exception as error:
        logger.info("no such todo")
        raise HTTPException(status_code=404, detail="Нет такой todo") from error
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/generate")
async def generate(count: Union[int, None] = None, token: Union[str, None] = None,
                   database: Session = Depends(get_db)):
    """Autocreate todos
    """
    # pylint: disable=unused-variable
    if count:
        for i in range(count):
            title = randint(1, 15)
            todo = models.Todo(title=title, tag=random.choice(["personal", "studies", "plans"]))
            database.add(todo)
            database.commit()
    else:
        title = randint(1, 15)
        todo = models.Todo(title=title, tag=random.choice(["personal", "studies", "plans"]))
        database.add(todo)
        database.commit()
    return RedirectResponse(url=app.url_path_for("home") + f"?token={token}", status_code=status.HTTP_303_SEE_OTHER)


@app.get("/export")
async def export_todo(database: Session = Depends(get_db), token: Union[str, None] = None):
    """Export todo from xlsx file
    """
    data = pd.DataFrame.from_dict({"id": [i[0] for i in
                                          database.query(models.Todo.id).all()],
                                   "title": [i[0] for i in
                                             database.query(models.Todo.title).all()],
                                   "details": [i[0] for i in
                                               database.query(models.Todo.details).all()],
                                   "completed": [i[0] for i in
                                                 database.query(models.Todo.completed).all()],
                                   "tag": [i[0] for i in
                                           database.query(models.Todo.tag).all()],
                                   "creation_date": [i[0] for i in
                                                     database.query(models.Todo.creation_date).all()],
                                   "completion_date": [i[0] for i in
                                                       database.query(models.Todo.completion_date).all()],
                                   "fullname": [i[0] for i in
                                                database.query(models.Todo.full).all()]
                                   })

    data.to_excel('export_data.xlsx', sheet_name='todos', index=False)
    RedirectResponse(url=app.url_path_for("home") + f"?token={token}", status_code=status.HTTP_303_SEE_OTHER)
    return FileResponse("export_data.xlsx")


@app.get("/import")
async def import_todo(token: Union[str, None] = None, database: Session = Depends(get_db)):
    """Import xlsx file to todo 
    """
    try:
        if token != "None":
            parent = tkinter.Tk()
            parent.overrideredirect(0)
            parent.withdraw()
            file_types = [('xlsx files', '*.xlsx'), (
                'All files', '*')]
            file_name = filedialog.askopenfilename(title='Select a file',
                                                   filetypes=file_types, parent=parent)
            data = pd.read_excel(file_name)
            data_frame = pd.DataFrame(data, columns=['id', 'title', 'details',
                                                     'completed', 'tags', 'creation_date',
                                                     'completion_date', 'fullname'])

            for i in data_frame.values:
                if i[3]:
                    if i[7] != '':
                        new_todo = models.Todo(id=i[0], title=i[1], details=i[2],
                                               completed=i[3], tag=i[4], creation_date=i[5],
                                               completion_date=i[6], full=i[7])
                    else:
                        new_todo = models.Todo(id=i[0], title=i[1], details=i[2],
                                               completed=i[3], tag=i[4], creation_date=i[5],
                                               completion_date=i[6])
                else:
                    new_todo = models.Todo(id=i[0], title=i[1], details=i[2],
                                           completed=i[3], tag=i[4], creation_date=i[5],
                                           completion_date=None)
                    database.add(new_todo)
            database.commit()
            return RedirectResponse(url=app.url_path_for("home") + f"?token={token}",
                                    status_code=status.HTTP_303_SEE_OTHER)
        else:
            logger.info("Not permitted")
            return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    except Exception as error:
        return RedirectResponse(url=app.url_path_for("home") + f"?token={token}", status_code=status.HTTP_303_SEE_OTHER)
        logger.info(error)


@app.get("/importlist")
async def import_list(request: Request):
    logger.info(imported_files)
    return templates.TemplateResponse("importlist.html", {"request": request, "imported": imported_files})


@app.get("/func")
async def new_page(request: Request, token: Union[str, None] = None):
    """Page with import and export
    """
    return templates.TemplateResponse("page.html", {"request": request, "token": token})


@app.post("/login")
async def authorize(username: str = Form(...), password: str = Form(...)):
    """Page with import and export
    """
    if username and password:
        logger.info(f"[LOGIN]: {username}, {password}")
        tkn = Authorization.check_user(username, password)
    else:
        logger.info(f"[ERROR]: empty value(s): '{username}', '{password}'")
    # return {"username": username, "password": password}
    return RedirectResponse(url=app.url_path_for(name="home") + f"?token={tkn}", status_code=status.HTTP_303_SEE_OTHER)

@app.get("/registerpage")
async def authorize(request: Request):
    """Page for registration
    """
    return templates.TemplateResponse("register.html", {"request": request})
@app.post("/register")
async def authorize(request: Request, username: str = Form(...), password: str = Form(...)):
    """Registration and authorization
    """
    if username and password:
        logger.info(f"[REGISTER]: {username}, {password}")
        tkn = Authorization.add_user(username, password)
        if tkn == 0:
            return templates.TemplateResponse("register.html", {"request": request,
                                                        "tkn": tkn})
    else:
        logger.info(f"[ERROR]: empty value(s): '{username}', '{password}'")
    return RedirectResponse(url=app.url_path_for(name="home") + f"?token={tkn}", status_code=status.HTTP_303_SEE_OTHER)

@app.get("/choose/{todo_id}")
async def choose_image(request: Request, todo_id: int, token: Union[str, None] = None, database: Session = Depends(get_db), image : str = None):
    """Choose from uploaded images
    """

    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if image != None:
        todo.image = image
        database.commit()
        return RedirectResponse(url=app.url_path_for(name="home") + f"?token={token}",
                                status_code=status.HTTP_303_SEE_OTHER)
    todos = database.query(models.Todo).filter(models.Todo.image != "")
    return templates.TemplateResponse("gallery.html", {"request": request, "todo_by_id": todo, "todos": todos, "todo_id": todo_id})


@app.get("/image/{todo_id}")
async def upload_img(request: Request, todo_id: int, database: Session = Depends(get_db)):
    """Import xlsx file to todo
    """


    try:
        parent = tkinter.Tk()
        parent.overrideredirect(0)
        parent.withdraw()
        file_types = [('All files', '*'), ('jpg files', '*.jpg'), ('png files', '*.png'), ('jpeg files', '*.jpeg')]
        file_name = filedialog.askopenfilename(title='Select a file', filetypes=file_types, parent=parent)
        #logger.info(file_name)
        todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()

        image = Image.open(file_name)
        image.load()
        commandlisten = ("pwd")
        path = subprocess.run(commandlisten, capture_output=True, text=True)
        mypath = path.stdout
        logger.info(f"PATH: {mypath}")
        path_pic = mypath[:-1] + "/static/images/"
        pictures = os.listdir(path_pic)
        check_pic = 0
        current_pic = 0
        duplicates = []

        while check_pic < len(pictures):
            if current_pic == check_pic:
                current_pic += 1
                continue
            try:
                #difference_images(file_name, os.path.join(path_pic, pictures[check_pic]))
                #print(f"{difference_images(file_name,os.path.join(path_pic, pictures[check_pic]))}")
                logger.info(f"awesome path: {file_name}, {path_pic}, {pictures[check_pic]}")
                dif = difference_images(file_name, os.path.join(path_pic, pictures[check_pic]))

                if dif:
                    todo.message += [dif]
                #todo.message = difference_images(file_name, os.path.join(path_pic, pictures[check_pic]))

                check_pic += 1
            except IndexError:
                logger.info("POSHEL NAXUY")
                return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
                break

        todo.image = mypath[:-1] + "/static/images/" + str(todo.id) + ".jpg"
        image.save(todo.image)


        todo.image = "http://127.0.0.1:8000/static/images/" + str(todo.id) + ".jpg"
        database.commit()

        logger.info(f"duplicates id: {duplicates}")


    except Exception as error:
        logger.info(error)
    return templates.TemplateResponse("edit.html", {"request": request,
                                                    "todo": todo, "tags": models.Tags,
                                                    "fullname": models.Fullname})




@app.post("/delete_all")
async def todo_delete_all(
        database: Session = Depends(get_db)):
    """Complete todo
    """
    todos = database.query(models.Todo).all()
    try:
        for todo in todos:
            logger.info(f"Deleting todo: {todo}")
            database.delete(todo)
            database.commit()
            path = subprocess.run("pwd", capture_output=True, text=True)
            folder = path.stdout[:-1] + "/static/images/"

            for filename in os.listdir(folder):
                file_path = os.path.join(folder, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print('Failed to delete %s. Reason: %s' % (file_path, e))
    except Exception as error:
        print(error)
        raise HTTPException(status_code=404, detail="Нет такой todo") from error
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/filter")
async def filter_studies(request: Request, database: Session = Depends(get_db), tag: str = "personal"):
    todos = database.query(models.Todo).filter(models.Todo.tag == tag)
    if todos.count() != 0:
        return templates.TemplateResponse("filter.html", {"request": request,
                                                          "todos": todos.all(), "tags": models.Tags,
                                                          "fullname": models.Fullname})
    else:
        return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@app.get("/datefilter")
async def date_filter(request: Request, date: str, database: Session = Depends(get_db)):
    check_date = date.split("-")[2] + "-" + date.split("-")[1] + "-" + date.split("-")[0]
    todos = database.query(models.Todo).filter(models.Todo.creation_date >= check_date)
    for todo in database.query(models.Todo).all():
        if not (todo.creation_date is None):
            logger.info(f"[AMOUNT]: {todo.creation_date} {check_date}")
    return templates.TemplateResponse("filter.html", {"request": request,
                                                      "todos": todos.all()})
